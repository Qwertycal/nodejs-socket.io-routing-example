const Router = require('express').Router

const router = new Router();


router.post("/message", (req, res, socket) => {
    console.log('req.message:', req.body.message);
    console.log('req.user:', req.body.user);
    // console.log('req.animal_congig:', req.animal_congig);
    const io = req.app.locals.io;

    // socket.emit("FromAPI", 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    io.emit("FromAPI", { avatarURL: 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d5', userName: req.body.user, message: req.body.message });
    res.send({ avatarURL: 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d5', userName: req.body.user, message: req.body.message } ).status(200);
});

module.exports = router