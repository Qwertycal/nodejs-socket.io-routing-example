const app = require('express')()
const server = require('http').Server(app);
const cors = require('cors');
const io = require('socket.io')(server)
const bodyParser = require('body-parser');
require('dotenv/config');
const messagesRouter = require("./routes/messages");

const port = process.env.PORT ;

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(messagesRouter);

server.listen(port, () => {
    console.log(`Listening on port ${port}`);
});

app.locals.io = io;
